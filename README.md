# LLSD for Rust
LLSD is a data encoding scheme for structured data.
This crate provides a pure Rust implementation of a LLSD serializer and deserializer.

## Format
### Specification
No formal spec was followed when implementing this crate, however there was an attempt to standardize LLSD in 2010 ([IETF link](https://www.ietf.org/archive/id/draft-ietf-vwrap-type-system-00.txt)) which should probably be analyzed more in depth in the future.

This implementation is based on the Second Life virtual world [Wiki page on LLSD](http://wiki.secondlife.com/wiki/LLSD) and inspection of the [LibOpenMetaverse](http://lib.openmetaverse.co/wiki/Main_Page) implementation to resolve a couple of ambiguities.

### Overview
Any value is either an instance of a scalar or container type.

The two container types are `Map` and `Array`, each instance holds a collection of values.
The scalar types are: `Boolean`, `Integer`, `Real` (float), `UUID`, `Date`, `Uri`, `Binary`, `Undefined` (absence of value).

Conversions between many of the scalar types are well defined, however some conversions are invalid.

## Implementation notes

- XML: Binary encoding only BASE64, decoding only BASE16 and BASE64 but no BASE85.
- This API is not stable yet.

## Legal Information
### License
```
Copyright (C) 2017-2018  Leonardo Schwarz <mail@leoschwarz.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

### Disclaimer
Second Life is a trademark of Linden Research, Inc.

Teleport Lab and LLSD for Rust are not affiliated with or sponsored by Linden Research.
