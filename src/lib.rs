// LLSD for Rust - Parser and Writer for the LLSD format.
// Copyright (C) 2017-2018  Leonardo Schwarz <mail@leoschwarz.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate byteorder;
extern crate chrono;
extern crate data_encoding;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate lazy_static;
extern crate quick_xml;
extern crate regex;
extern crate url;
extern crate uuid;
// TODO: Why is this public in the first place?
extern crate xml as xml_crate;

pub mod binary;
pub mod data;
pub mod xml;

mod autodetect;

pub use autodetect::{read_value, PREFIX_BINARY};
