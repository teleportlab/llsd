use chrono::{DateTime, Utc};
use std::str::FromStr;
use url;

pub type Boolean = bool;
pub type Integer = i32;
pub type Real = f64;
pub use uuid::Uuid;
pub type Date = DateTime<Utc>;
pub type Binary = Vec<u8>;

/// A wrapper type for the `url` crate `Url` struct.
///
/// This mainly takes into account that in LLSD empty/absent URL values are possible,
/// and also includes a variant unparseable urls.
#[derive(Clone, Debug)]
pub enum Url {
    Ok(url::Url),
    Empty,
    Invalid(String, url::ParseError),
}

impl Url {
    pub fn into_string(self) -> String {
        match self {
            Url::Ok(u) => u.into_string(),
            Url::Invalid(s, _) => s,
            Url::Empty => "".to_string(),
        }
    }

    pub fn ok(self) -> Option<url::Url> {
        match self {
            Url::Ok(u) => Some(u),
            _ => None,
        }
    }
}

impl<'a> From<&'a str> for Url {
    fn from(s: &'a str) -> Self {
        if s.len() == 0 {
            Url::Empty
        } else {
            match s.parse() {
                Ok(u) => Url::Ok(u),
                Err(e) => Url::Invalid(s.into(), e),
            }
        }
    }
}

impl From<String> for Url {
    fn from(s: String) -> Self {
        s.as_str().into()
    }
}

impl PartialEq for Url {
    // TODO: Consider if there is a better way to implement this (or skip the impl.)
    fn eq(&self, other: &Url) -> bool {
        let left = self.clone().into_string();
        let right = other.clone().into_string();
        left == right
    }
}

impl FromStr for Url {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(s.into())
    }
}
